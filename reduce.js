function reduce(elements,cb,startingValue) {
    if(startingValue !== '[]'){
        startingValue = elements[0];
    }
    let Sum = startingValue;
    for (i=1; i<elements.length; i++) {
        Sum += cb(elements[i],startingValue,elements);
    }
    return Sum;
}

module.exports = reduce;