function find(elements,cb){
    let num;
    for(i=0; i<elements.length; i++){
        if ( cb(elements[i],num) ){
            return elements[i]
        }
    }
}

module.exports = find;