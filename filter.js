function filter(elements,cb){
    let R = [];
    for (i=0; i<elements.length; i++){
        if (cb(elements[i]) ) {
            R.push( cb(elements[i],elements) )
        }
    }
    if(R.length >0) return R
    else return []
    
}

module.exports = filter;