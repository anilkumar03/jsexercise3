const flatten = require("../flatten");


const nestedArray = [1,[2],[[3]],[[[4]]]];

if(nestedArray.length ===0) console.log([])
else{
    let result = [];
    for (i=0; i<nestedArray.length; i++) {
        a = flatten(nestedArray[i],nestedArray);
        result.push(a);
    }
    console.log(result);
}
