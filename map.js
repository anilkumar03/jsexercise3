function map(elements,cb) {
    let newArray = []
    for (i=0; i<elements.length; i++) {
        newArray.push(cb(elements[i],i,elements));
    }
    return newArray;
}

module.exports = map;